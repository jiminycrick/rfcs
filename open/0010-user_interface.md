- Feature Name: User_Interface
- Start Date: 2018-07-16
- Tracking Issue: https://gitlab.com/veloren/rfcs/issues/11

# Summary

[summary]: #summary

Veloren should have it's own API for the game's **GUI** (Graphical User Interface). This RFC explains the goals and the technical design of Veloren's GUI.

# Motivation

[motivation]: #motivation

Every game needs an **UI** (User Interface) in order to achieve interaction with the user. Such a system covers a wide variety of use cases :

- Main menu
- Settings panel
- HUD (Head-Up Display)
- Multiplayer chat

We need to provide Veloren developers with an API that fits the development needs and different goals of the project. Provided this is our API that will be optimised to match the project's performance, this will give us the flexibilty we need in order to achieve such an important part of the game.

Building and modding the UI of the game should be easy and well-documented, hence the creation of our own UI system.

Also, since we're making Veloren from scratch, it makes sense to have an UI built from the ground up, so we can provide a referance example of what a Rust game made from scratch would look like, both performance-wise and in terms of design choices.

# Guide-level explanation

[guide-level-explanation]: #guide-level-explanation

## Layout

The UI system as proposed in this RFC follows a design that is willingly kept simple and meant for rapid prototyping. It is based around anchors and box layout.

In order to work properly, a GUI system needs to have some sort of hierarchy. This main rule is covered in the design by the use of the box layout system : an UI element is either a **Widget** or a **Container**, each container is able to be the parent of other elements (i.e. Widget or container), resulting in a tree-like hierarchy (HTML-like).

**Widgets** are elements that you can actually see on the screen.
**Containers** are elements that contains other elements, but you can't actually see them.

In each one of the container, we can define a set of variables that are going to be conditioning the placement of the children elements. It is really important to note that in this case, _parents are responsible for the positionning of their children if they dont set their position themselves using UIPosition (anchors and origins)_.

## Positionning

Even though positionning should usually be computed by constraints, there is a special case where placement can be done in **absolute coordinates**. This is done using **anchors** and **origins**.

**Anchors** are two-dimensional coordinates, expressed in screen space, contained in the range [(0,0);(1,1)], starting from the top-left of the screen. They act like ... Well, anchors. They grab the content they are assigned to, and act as the strongest constraint possible, so that the content will stick to the position it is assigned.

**Origins** are two-dimensional coordinates, expressed in local component's space, that describes where the anchor is attached. For example, an origin of (1.0, 0) would mean that the anchor is attached to the top-right corner of the element, thus letting the anchor place said corner accordingly.

![Anchors and Offsets](../misc/ui_rfc/AnchorsAndOffsets.png)

Positionning should be optional. In case there is

## Data storing

In order to save the state of such a complex system, we need a good way to store, access and modify data. Thankfully, web developers have already prepared everything we need! This RFC proposes that Veloren implements its own [Flux Pattern](https://facebook.github.io/flux/docs/overview.html), in a similar way as Vuex, but in pure Rust (so, preserving performance and safety across threads).

The core idea of Flux is to separate the state of the UI in a different modules, and restrict read/write access on the state itself using actions,getters and mutations.

Such a pattern allow us to create the UI and manage the state separately, while effectively making changes in the game's state is possible by creating a bridge between Flux and the game's core using **Watchers**. These would launch actions on the game's state when a mutation is done on the store, and eventually we could follow the game's state and have a reactive UI.

## Reactive rendering

The UI should be reactive, meaning that it would change it's content on-the-fly when the store is affected by mutations. To integrate well with the rest of the UI system and maybe allow for internationalisation, it would be possible to use a templating engine such as [Handlebars](https://crates.io/crates/handlebars) or [Mustache](https://crates.io/crates/mustache), and start handling text from there.

# Reference-level explanation

[reference-level-explanation]: #reference-level-explanation

## Data storing

In order to separate the work on the UI, and because a rusty implementation of the Flux pattern might be something that a lot of WASM devs might be interested in and will surely gain attention in the future, we should put it in a separate crate.

We should be able to make use of Rust's [macro system](https://doc.rust-lang.org/book/second-edition/appendix-04-macros.html) to easily declare state, actions, mutations and getters, splitting it into modules. For example, the API of a store module would look like this :

**Note: this is a very early API and is subject to change:**

```rust
struct PersonState {
    pub name: String,
    pub surname: String,
    age: u32,
}

impl PersonState {
    pub fn new() -> Self {
        PersonState {
            name: "".into(),
            surname: "".into(),
            age: 0,
        }
    }
}

pub fn get_full_name(state: &PersonState) -> String {
    format!("{} {}", state.name, state.surname)
}

pub fn is_adult(state: &PersonState) -> bool {
    state.age >= 18
}

pub fn set_age(state: &mut PersonState, payload: u32) {
    state.age = new_age;
}

pub fn apply_birthday(state: &PersonState, commit: StoreDispatch) {
    commit("set_age", state.age+1);
}
```

Using the store module in the store would then look like:

```rust
struct MyPersonWidget {
    #[bind_getter(PersonState/get_full_name)]
    fullname: Getter<String>,
    #[bind_getter(PersonState/is_adult)]
    adult: Getter<bool>,
}

fn main() {
    let store = Store::new(
        modules: vec![
            store_module!(
                PersonState,
                getters!(get_full_name, is_adult),
                mutations!(set_age),
                actions!(apply_birthday),
            ),
        ],
        namespaced: true,
    );

    let mut my_person_widget = MyPersonWidget::new();
    let mut my_other_person_widget = MyPersonWidget::new();
    my_person_widget.bind_store(store);
    my_other_person_widget.bind_store(store);

    (0..18).for_each(|_| store.dispatch("PersonState/apply_birthday"));
    println!("{} {}",
        my_person_widget.adult.get_val(),
        my_other_person_widget.adult.get_val()
    ); // Should display "true true"


}
```

## Positionning

An element is defined by its position and size. The position can either be absolute (based on anchors and origins as described earlier), or parent-driven.

Each box element should apply a constraint to its children, which are solved by using the cassowary algorithm (see #prior-art)

**Needs more reference on how to handle placement**

## UI files

Loading the UI should be easy to do, and splitting the UI into files is a good way to go about managing such a complex system.

I propose that we use the [RON file format](https://github.com/ron-rs/ron) to store Rust structs that would represent modules, i.e. parts of the UI. It would then be trivial to link the correct files, and allow for easy modifications without breaking unrelated parts of the UI.

Example of how we would represent a really basic start menu

```RON
(
    children: [
        Label(
            text: "Veloren",
            position: UIPosition(
                anchor: (0.5,0.3),
                origin: (0.5,0.5),
            ),
            size: (0.5, 0.2),
        ),
        Panel(
            children: [
                Button(
                    id: "BtnStart",
                    text: "Start",
                ),
                Button(
                    id: "BtnQuit",
                    text: "Quit",
                ),
            ],
            position: UIPosition(
                anchor: (0.5, 0.5),
                origin: (0.5, 0.5),
            ),
            size: (0.25, 0.25),
            children_display: Display::Flex,
        )
    ]
)
```

As you can see, position is an Option because we don't know if we are in root-mode (absolution positionning using UIPosition), or following the parent (box layout).
Also, the ID on the widget would be used to bind events to functions.

**Needs to show the integration of a templating engine maybe ?**

# Drawbacks

[drawbacks]: #drawbacks

Although having an UI is necessary, as it represents a big part of the user experience in a game, the design of the custom framework is of course subject to discussion.

Having an UI based on origins and anchors like presented here has a few drawbacks that I think would not be negligible :

## Responsiveness

Having an anchor-based system system allows for development one one screensize, but would of course not look the same on different screen resolutions, sometimes making the UI unclear on small screens, whereas a full-box layout would integrate responsiveness as a core functionnality.

## Extensibility

# Rationale and alternatives

[alternatives]: #alternatives

The given design, being entirely custom, might not be the best in terms of performance, usability and maintainability. For the sake of objectivity, other designs have been elaborated and might be implemented instead of the design mentionned above :

## Deploying Veloren UI as a wrapper around Conrod

Another possibility is to use Conrod as a backend and provide our own API on top of it. This has multiples pros and cons :

### Pros

- Simplicity! This design allows us to stop reinventing the wheel and start relying on a crate that's been under active development for about 2 years.

### Cons

- Performance. We don't have total control over what's hapenning under the hood, and a lot of unnecessary work might be done.

- Dependency. As good, stable and usable as Conrod may be, it is still one more dependency and may be the source of problems with other crates that the project cannot live without (e.g. we already had a compatibily problem with Glutin).

It is left to the team to decide whether they choose to spend time deploying their own framework or build a custom wrapper around Conrod.

## Full box-based layout model

Instead of designing Veloren's UI as a root-based, absolute-positionned system with box model functionnality inside, we could instead go for a full box-based layout.

### Pros

- Consistency. Box layouting is easy to implement because every component acts as a container and an UI item, thus making it a lot easier implementation-wise. We would just be using a constraint solving algorithm in the inside.

### Cons

- Complexity. It seems like having anchors available is a good way to create a global UI and place items somewhere on the screen without caring for other UI components. Box-layouting something requires that you care where other items are placed, or use absolute positionning which is essentially what we're implementing here.

# Prior art

[prior-art]: #prior-art

- Papers: Are there any published papers or great posts that discuss this? If you have some relevant papers to refer to, this can serve as a more detailed theoretical background.

In case we decide to not take inspiration from already-existing crates, and start working on our own implementation, box model constraints can be resolved using the cassowary algorithms, which have been implemented in Rust over at [cassowary-rs](https://github.com/dylanede/cassowary-rs).

As mentionned before, we have a lot of existing crates to base our work upon :

- [Conrod](https://github.com/PistonDevelopers/conrod) (GUI system, based on gfx-rs). This is the reference implementation of a GUI system that has a lot of great ideas, and we might steal ideas from their architecture.
- [Imgui-rs](https://github.com/Gekkio/imgui-rs) (GUI system (originally written in C, with Rust bindings), works with OpenGL). Some interesting reference, we might want our own API to somewhat look like what the original imgui provides.
- [Amethyst](https://github.com/amethyst/amethyst) (Rust Game Engine, has an embedded UI system). Although the UI's code under the hood is quite messy, the API is usable and has the great advantage to handle the RON format correctly, which is one of the core features of this RFC.

Those crates are interesting starting point, and provide good example of what our UI will tend to look like.

On the topic of state patterns for UI, relevent explanations are available on the [Flux pattern](https://facebook.github.io/flux/docs/overview.html)'s overview page. It is implemented in both React.js and Vue.js ([Vuex](https://vuex.vuejs.org/))

# Unresolved questions

[unresolved]: #unresolved-questions

Some questions remains unsolved and open to discussion:

## Design

- We still have to discuss whether we implement the root window in absolute position (anchor), following a full box model, or force ourselves to work around Conrod.

## Implementation

- What would the impact on performance of all different methods be ? We need to have some benchmarks running to compare and judge by ourselves if implementing everything from scratch is the only true solution to our UI problem.

## Related future issues

- 3D GUI handling (for example UI that appears on top of players for e.g. HP Bars / Name / Level) is willingly left out of this RFC, and even though we should prepare the UI system for this particular use case, it will be further discussed in another RFC.
- A cool UI require animations (and tweening) ! We need to think about the way we could handle such feature, but this is out of scope for now.
- Internationalisation. Not anywhere in the near future, but the UI might eventually need to be translated in different languages, so that the game can be played in every country !
