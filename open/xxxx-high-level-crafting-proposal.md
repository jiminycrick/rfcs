# Crafting Foundations:

1.  Turn crafting into something that requires thought, not excessive time to gather materials. Most modern crafting systems are more of a scavenger hunt for materials than crafting. Make it more about being at the crafting table than looking for materials.

2.  A recipe is not just a list of materials, it is also the process of which you use them.  Think of cooking a meal - a spaghetti recipe is not just water, noodles, tomatoes, spices but what you do with the noodles, how long you cook them, how long and at what temperature to roast the tomatoes and so forth.

3. Progression should not be an experience bar that gets larger, but knowledge and understanding of a system. Like in real life, crafting requires iteration, experience, and experimentation.  Allow the player to make mistakes but learn from them.  

4. Make someone who enjoys crafting explore/interact with the world to also get better.  Veloren is not a crafting simulator, but an adventure game. Material descriptions that have clues of how they can be used, find unique recipes/techniques from factions, apprentice under a blacksmith that can teach you something, find a rune in a dungeon and make a note of it, when a crafter gets good enough, have NPC’s hire them to make specialty items.

5. Success and failure on crafting should not be solely RNG but from following some kind of cue from the process, be it visual, audio, text, or form.

6.  Add some kind of player interaction that is applicable to crafting but not an arbitrary minigame.  Vague mini-games are not a testament of a player’s crafting ability but are a non impactful piece of flair that tries to look like depth.  Think puzzle versus QTE prompt.

7. Incentivise experimentation through increased progression opposed to just looking up things on the internet.  With the birth of the internet, crafting in games becomes looking up the recipe to have full description of how to craft something.  This indicates that the process is tedious enough that it does not warrant exploring the system.  If the player has positive stimulus and increased progression from experimentation, it incentivises them to continue to experiment.  This can be as simple as applying an experience multiplier for every failed attempt or if even a failed attempt leads to the beginning of other crafting recipes.  Once the player discovers the crafting recipe through iteration, that experience multiplier boosts the crafting experience more and they suddenly have the beginning knowledge of other crafting recipes, encouraging the player to keep experimenting.





# Proposal:

Crafting is split into 3 distinct stages, material preparation, form manipulation, and final treatment.  For simplicity sake we will call them preparation, form, and treatment.

1. Preparation:  A player will gather their materials and place them in a preparation machine, be it a furnace, loom, magical device, potion pot, tanning rack, whatever.  The material preparation will require knowledge or a recipe to make the crafting material they want.  For example, to make steel, a player will add iron ore and charcoal to a furnace.  Material descriptions can lend clues to potential combinations.  Iron might say “Hard iron, I wonder what would happen if I mixed it with something crumbly…” while charcoal might say “Burnt and crumbly, I wonder if I mix it with something hard…”  They will then have to “prepare” the material, following a recipe and looking for visual cues like color of the furnace or a particle effect that will occur with any material in the preparation machine.  Going back to the steel and furnace example, the player will “pump the bellows” until the furnace gets to a specific color.  The furnace will go through all of the colors of the spectrum for any material, but to make steel, you have to reach a specific temperature or “color” and maintain it for a short duration.  If too cold, there can be a text cue that says “It doesn’t look like it formed”.  If too hot, a text cue can say “It looks ruined!”.  Keep in mind, all of these steps don’t require the player to have a gate-kept crafting recipe or book.  They can gain this knowledge through experimentation, apprenticeship, or watching a blacksmith make items. 

Concept Summary: 

- Player must have knowledge of the material recipe of steel
- Player can get clues from item description on mixtures
- Player must interact with the preparation machine to reach a desired temperature
- Player must know how long to hold the temperature

2. Form:  The second step of crafting will be form.  We will use the blacksmithing example from above to make a sword.  When making a “form” for a sword, a 3x3 grid will pop up.  A recipe might say “Starting from the base, strike once from the base to the tip in the middle of the blade.  Next- from the base of the blade working up, strike once along the left side and then strike once along on the right.” What the player will see is the 3x3 grid and will require them to start from the bottom and click along the middle squares, back to bottom and along the left, back to bottom and along the right.  The player feels like they are “forming” a sword by shaping it based on the geometry of the sword and following directions.  In reality, it is simply a code with numbers assigned 1 through 9.  So the particular code would be [8, 5, 2, 9, 6, 3, 7, 4, 1] .  If the player messes up, there can be a text cue that might say “That’s not right, let me start over…”.  So if a player didn’t have a recipe, they could experiment by trying to guess codes with iteration.  To add further difficulty there can be a time limit for this stage.  The codes can try to mimic geometry of specific items or techniques like steel folding.  If the grid is expanded out to 5x5 (to keep a center line) they can better mimic the geometry but the codes might get crazy.
    
    
Concept Summary:

- Recipes are a verbal explanation, not a material’s list
- Iteration can lead to discovery
- Player requires interaction and thought, not just a simple minigame or QTE event

3. Treatment:  This is the final stage of the crafting process.  The player must choose the proper “treatment” fluid material and place the finished product in there for a specific amount of time.  Indication of proper treatment might be a visual cue like steam or a bad visual cue like fire.  Each material type will have a defined treatment fluid and require a specific amount of time in treatment.  
    
Concept Summary:

- Player must have knowledge of the material or recipe
- Player must interact with an actual system
- Player must be engaged and watching for cues

# Other Ideas:  
Specific materials in the preparation can be added to any metal but will bump the “temperature” requirement in either direction.  For our steel example, maybe we threw in some fantasy material to add fire damage to the sword (there would also be a draw back like take extra damage), maybe that addition would change the required preparation temperature.

Maybe forming techniques can be learned and applied to any form.  Think of the japanese steel folding.  The “folding” code can be placed in before the blade form code which will add damage but reduce blocking (still want to maintain drawbacks, not just benefits for balance sake). 

Treatment can be like actual heat treating and swing base stats or capabilities in either direction.  For example - If you quench steel really quickly, you get a form of steel that is super hard but super brittle so the damage may increase but the blocking capability might be decreased.  If you barely quench the material, the steel would be soft so less damage but more blocking potential.  Essentially it would be a range of stats with quenching swinging the stats far from one side to the other.

For magical items, maybe you find a rune drawing in a cave.  Forming would require you to replicate the rune shape in the forming grid.

