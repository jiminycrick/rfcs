- Feature Name: Leveling System (or lack thereof)
- Start Date: 7/19/18
- RFC PR: 
- Rust Issue: 

# Summary
[summary]: #summary

Instead of using a conventional leveling system that consists of
- doing things to gain experience points (XP)
- getting enough XP to gain a level
- needing more XP for the next level
we can completely remove levels, and instead use the world to train the player. This can be accomplished by having trainers and sages in cities, and ties into a partially hidden proficiency system where the player can get better at various non-combat aspects of the game by simply doing things.

# Motivation
[motivation]: #motivation

This project is beginning to revolve around the player being the most important aspect of the game. When you start to include levels and XP and damage numbers in a very open-ended game like this, the numbers begin to gain importance over the player. However, the player still needs to be able to get stronger in the absence of experience points.

The trainer allows us to train the player, without forcing them to grind for XP. If trainers to only a few training sessions, the trainer system can also encourage the player to explore to find more cities and towns with better trainers. The proficiency system provides an intuitive way for side skills to "level up" without giving numbers an influence over how the player plays the game.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

Every city will have a trainer, just as they will have various shops and an inn.

The trainer will "train" you for a fee, which will boost your core stats (Intelligence, Dexterity, and Strength) by 1 plus whatever bonuses their classes provides. Each time you train, the fee for the next "train" will be higher. Whether they also boost the mana pool is up for debate. They can also "teach" you skills from the skill tree, and once you've learned the skill you can train more in the skill by paying more money.

Each trainer will have a limit to how what they can teach you. Once you reach the limit they will say something along the lines of "I cannot teach you any more; you have surpassed me. You must search for someone stronger to learn more." This limit will be a simple number of trains (say 5 or so) so that the player will never overpower a trainer just by training with another one. One "training" count could be either training your core stats and training in a skill.

Proficiencies, on the surface, are very simple: the more you do something, the better you get at it. Proficiencies are essentially behind-the-scenes "levels" that define how well you can do most things in the game. With a higher proficiency in:
- mining: you would get more ore from mining
- blacksmithing: you would get better buffs on crafted armors/weapons
- climbing: your stamina would go up
and so on. These numbers would all be hidden, and you would only see "Proficient X" in a menu. What menu you would see this in is undetermined, since we don't know what menus we will have. If we were to use Cube World's menu as an example, the proficiencies would be in the inventory screen right under the stats text box.

There are different stages of proficiency:
- Mediocre
- Good
- Proficient
- Expert
As expected, the more you do an activity the higher stage of proficiency you will get.

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

TBD

# Drawbacks
[drawbacks]: #drawbacks

Players may want to see what level their character is. It may be a matter of personal pride, or an easy way to compare themselves to other players. Seeing yourself level up or telling someone "I'm level 100" is satisfying.

# Rationale and alternatives
[alternatives]: #alternatives

The main alternative to a training/proficiency system is experience points. You can gain experience points towards your character level by defeating enemies, and gain experience points towards your mining (as an example) level by mining. However, this also attracts the issue of level caps.
- Do we want to cap the player's level, or let them level up infinitely?
- Do we want to cap side skills' levels, or let them level up infinitely?
Each of which poses their own issues and a fairly large list of pros and cons. With something much simpler, we don't have to solve that problem because it no longer is one.

# Prior art
[prior-art]: #prior-art

There are many games that do not use a leveling system to better the player. Many games allow the player to get better through simply equipping better equipment. Terraria and TLoZ: BotW are notable examples of this: the player never gains "XP", and is never limited to what they can wear or equip. Instead, it is what you equip that makes you more powerful.

Dwarf Fortress is the best (and one of the only, to my knowledge) examples of the proficiency system. Each dwarf starts with certain proficiencies aed preferences tied into their generated personalities, and will get better at any task by simply performing the task.

# Unresolved questions
[unresolved]: #unresolved-questions

TBD
