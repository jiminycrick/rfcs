- Feature Name: Lung_capacity_and_air_pockets
- Start Date: 2021-05-11
- RFC Number: xxxx
- Tracking Issue: xxxx

# Summary
[summary]: #summary

A suggestion for when water blocks can be removed and re-placed individually (if they can't already). Breath holding timer, as well as skill for increasing lung capacity. 3 new potions. One that increases speed/mobility while swimming, a lung capacity potion and an upgraded water breathing potion. An object and/or plant that removes water around it, creating a small zone that lets players and NPCs walk and breathe normally.

# Motivation
[motivation]: #motivation

Why are we doing this? What use cases does it support? What is the expected outcome?
Underwater Danari cities! Having to hold your breath underwater is obviously realistic while being able to swim underwater indefinitely isn't. Adds diversity to underwater dungeons and caves where some have more freedom in breathing and walking. Fighting underwater becomes more of a challenge and is more of a reason to invest skillpoints in swimming mobility, while not becoming a nuisance and the breathing zones allow for an extended time underwater. It's also a way to explain how travellers can stay in Danari cities.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

Explain the proposal as if it was already included in the game and you were explaining it to another player / developer. That generally means:

- Introducing new named concepts.

Lung capacity and holding your breath underwater. Because diving forever isn't realistic and the feature is coming eventually.
3 potions:
- A potion that increases lung capacity allowing you to swim underwater for longer.
- An upgraded version that gives you full water breathing capability
- A potion that increases your swimming speed and mobility.

An object that removes water around it in a sphere. A naturally occuring plant that has the same effect. Found in underwater caves and crevices

- Explaining the feature largely in terms of examples.

The potions create possibilites for extended excursions to water areas and fights underwater. The first version is mainly for exploring while the upgraded version is for not having to worry about air, especially useful during fights.
The air bubble allows players and NPCs to walk, breathe, exist as if above ground. This let's non-fish-people visit underwater cities and explore caverns and tunnels found underwater.
Air pockets in underwater dungeons and caves would be more sparsely placed as they get more and more difficult/deeper. This means the players and NPCs will need to think ahead and prepare before exploring long distances.

- Explaining how Veloren players and developers should *think* about the feature, and how it should impact the way they play Veloren. It should explain the impact as concretely as possible.

These additions allow for exploration underwater with ways to explore longer. Also increases diversity in caves with unique features, making underwater caves special and different from caves above ground. The air pockets explain how non-water breathing creatures can live in caves filled with water and Danari cities in oceans.

- If applicable, describe the differences between how existing and new Veloren players / developers would adapt to the changes.

Really my suggestions are additions of content and that make water content more interesting and challenging to explore, and I think would only incentivize people to explore the edges of the map more. I also think it would be a source of motivation for creating additional water content.

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

This is the technical portion of the RFC. Explain the design in sufficient detail that:

- Its interaction with other features is clear.
- It is reasonably clear how the feature would be implemented.
- Corner cases are dissected by example.

The section should return to the examples given in the previous section, and explain more fully how the detailed proposal makes those examples work.

Swimming needs more content, and this would add some and allow for more and as said, would encourage more too.
I'm sorry I don't really know what to write her. Please comment on this to direct me a bit.


# Drawbacks
[drawbacks]: #drawbacks

Why should we *not* do this?

This section is *really* important. An RFC is more likely to be accepted if it recognises its own weaknesses and attempts to consider them as part of the proposal.

Remember: Listing drawbacks doesn't mean an RFC won't be accepted. If anything, it gives developers the confidence to move forward with the RFC, knowing what impact it may have.

Balancing the distance between pockets relative to the difficulty of dungeons and caves may be messy and require a lot of finetuning to get the distances and sizes right. As well as finetuning how long characters can hold their breath.
NPCs pathfinding to find air pockets in order to survive will also be a challenge. And depending on how this is added to the game, either all non-water animals and NPCs have the same lung capacity or each have their own, which would require more work.

# Rationale and alternatives
[alternatives]: #alternatives

- Why is this design the best in the space of possible designs?
- What other designs have been considered and what is the rationale for not choosing them?
- What is the impact of not doing this?

The other design I imagine could work is not adding holding your breath in the first place, but this would be unrealistic and does not fit in the game.
I can see natural air pockets in underwater caves being similar but they would have to look natural and be spaced well. I do not think world generation is a good solution for this as it adds complications to the generation of caves.
Underwater areas remain unexplored, or people explore the underwater caves with air mechanics and are frustrated by there not being a way to travel long distances, to or from a place. Also makes adding and rationalizing exploring underwater content difficult as there is no good way to explore it.

# Prior art
[prior-art]: #prior-art

Discuss prior art, both the good and the bad, in relation to this proposal.
A few examples of what this can include are:

- Does this feature exist in other games or engines and what experience have their community had?

I am unsure if there are similar air pocket features in other games that aren't natural air pockets in caves. In Minecraft you can place a door or sign underwater and it removes the water in the same block(s), though this is much smaller and instantly placed. I know that Rapture is an underwater city in Bioshock and Bioshock 2 but I do not know if they have a similar feature as I haven't played the games.

Air pockets in underwater caves in real life do exist, but they are rather rare. And as I have understood how the sea level in the game works, any empty terrain blocks under the sea level are replaced with water. This would have to change if natural air pockets were to be incorporated as well (or together with this addition).
As for the potions, both Terraria and Minecraft have water breathing potions. The one in terraria lasts for 4 minutes and Minecraft has 2 versions, the base version (3 minutes) and an upgraded version (8 minutes).

As said, these games have water breathing potions which removes lung capacity entirely. My other suggestion is a potion that increases lung capacity. The Witcher 3 has a similar potion called Killer Whale. It doesn't have a long duration but swimming in the game is only for short durations. For example, jumping down a well, swimming through a short cave and already exiting the water. The Killer Whale potion doubles lung capacity mostly for quests where longer periods underwater is good to have or not having to focus on how much air is left. While the potion is similar, Veloren has much longer durations underwater as caves are very large and there is much more to explore.

Terraria also has a Flipper potion and item which let's you swim in water as you originally can't, you can only jump from the bottom and if you don't reach the air, well then you don't. As Veloren already has swimming I suggest the swimming potion instead increasing the swimming speed and mobility of the player/NPC.


# Unresolved questions
[unresolved]: #unresolved-questions

- What parts of the design do you expect to resolve through the RFC process before this gets merged?

Clarification of the design and my image in how it would be added to the game. Also perhaps explaining more why this is benefitial to the water content and Veloren in general.

- What parts of the design do you expect to resolve through the implementation of this feature before stabilization?

I expect the design of air pockets and lung capacity will be resolved during their creation. Air capacity resolves an unrealistic part of the game at the moment, and the air pockets solve not being able to explore for long because there is no air.

I expect what things that affect lung capacity and size of the air pockets to be resolved while the features are being created. I am sure a large part of the community will have things to add to the proposal and it's features.

- What related issues do you consider out of scope for this RFC that could be addressed in the future independently of the solution that comes out of this RFC?

I can imagine bugs about the air pockets and lung capacity not functioning properly arising. Balancing of the potions, lung capacity, and air pockets will also be worked on in the future. This RFC focuses more on the idea and initial implementation of the features.
