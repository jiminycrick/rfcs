- Feature Name: backend_builder_patterns
- Start Date: 2018-07-16
- RFC Number: 0006
- Tracking Issue: #6

# Summary
[summary]: #summary

I propose that we use a builder pattern when providing frontends with a way of constructing instances. This would apply to both `server` and `frontend` code, and the publicly-exposed types they use.

# Motivation
[motivation]: #motivation

Currently, the construction of types in both the `server` and `client` backends are inconsistent and awkward. Currently, we use variations on the `Type::new()` constructor method. However, this is limited for several reasons:

- Constructors tend to be long, wordy, and awkward to read

- Functions are 'static' (i.e: Rust does not support function overloading), leading to many unnecessary constructor arguments

- Constructors are annoying to refactor. When a change to the constructor is required in one part of the codebase, it often requires a change to every other location at which that constructor is used

Builder patterns are common across the Rust ecosystem. Their advantages include:

- **Composability**: The pattern can be adjusted to suit the needs of a situation without breaking the rest of the codebase

- **Readability**: Builder patterns are easy to interpret, even to beginner programmers, and provide a clear explanation of what each constructor argument is at the call sight

- **Flexibility**: Builder patterns do not need refactoring when an addition to the underlying type (or indeed one of its construction options) is needed

- **Idomatic Rust**: Rust's move-by-default type system makes the builder pattern the most idomatic choice for type construction

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

The builder pattern would be used extensively throughout the backend API for constructing anything from clients, entities, chunks, abstract instances like civilisations, and more. It would provide a consistent and easy to use method for constructing types that requires a developer to memorise only what they need to know to construct a type.

## Examples
[examples] #examples

Here are some examples of construction with the builder pattern.

```rust
// 'Full' construction
let my_entity = Entity::make()
    .with_name(&"Alice")
    .with_position((x, y, z))
    .with_velocity(vec3!(dx, dy, dz))
    .with_orientation(vec2!(45.0, 0.0))
    .finish();

// 'Partial' construction
let another_entity = Entity::make()
    .with_name("Bob")
    .with_velocity(vec3!(dx, dy, dz))
    .finish();
```

```rust
// Here, the order of construction methods is important. Therefore, the underlying type
// changes with each operation, thereby enforcing these ordering rules at a type level
let chunk_model = Chunk::make(vec3!(32, 32, 256))
    .now_generate_from::<TestGenerator>()
    .now_mesh()
    // Note that we *do not* need to finish construction with a .finish() method
    // because a .now_xxx() method is allowed to change the underlying type
    .now_model();
```

```rust
let client = Client::make()
    .with_remote("8.8.8.8:80")
    .with_conn_timeout(5000)
    .with_callback(ClientEvent::RecvMsg, |e| println!("Message received: {}", e.text()))
    .with_mode(ClientMode::Headless)
    .finish();
```

This pattern is best implemented for types that meet the following criteria:

- They are constructed in many locations in the codebase

- The form of construction may be different depending on the type's requirements

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

## Specific Rules

- Construction begins with a `Type::make()` function within the type. It returns a `TypeBuilder` type and can take one or more 'essential' parameters (i.e: parameters that are necessary to construct the type)

- To specify the type's state, a `.with_xxxx()` method is appended, where `xxxx` is the noun denoting the name of the state

- To perform a stateless action during construction (i.e: one that does not affect the state of the final type), a `.then_yyyy()` method is appended, where `yyyy` is a verb denoting the name of the action (i.e: `.then_log()` or `.then_gen_uid()`)

- To perform a stateful action during construction (i.e: one that does affect the state of the final type), a `.now_zzzz()` method is appended, where `zzzz` is a verb denoting the name of the action (i.e: `now_mesh()` or `now_compile()`). Note that it is fine for a `now_zzzz()` method to *change* the underlying type if necessary (for example, a `.now_mesh()` method may actually convert the underlying type to a `Mesh` type)

- To finish object construction, the `.finish()` method is called to transform the type into its final, finished form. Additionally, a `.now_zzzz()` method is permitted to perform the 'final' construction too.

- All methods other than `Type::make()` 'consume' `self` (i.e: take `self` with no reference or mutability qualifiers as their first argument)

## Implementation

Here is an example construction, borrowed from above.

```rust
let my_entity = Entity::make()
    .with_name(&"Alice")
    .with_position((x, y, z))
    .with_velocity(vec3!(dx, dy, dz))
    .with_orientation(vec2!(45.0, 0.0))
    .finish();
```

Here is the corresponding implementation that makes the above code possible.

```rust
// EntityBuilder acts as the 'template' for Entity construction
struct EntityBuilder {
    name: Option<String>,
    pos: Option<Vec3<f32>>,
    vel: Option<Vec3<f32>>,
    ori: Option<Vec2<f32>>,
}

impl EntityBuilder {
    // Utility methods. All of these are 'optional' and don't *need* to be called
    fn with_name(self, name: &str) -> EntityBuilder { self.name = Some(name.to_string()); self}
    fn with_position(self, pos: Vec3<f32>) -> EntityBuilder { self.pos = Some(pos); self }
    fn with_velocity(self, vel: Vec3<f32>) -> EntityBuilder { self.vel = Some(vel); self }
    fn with_orientation(self, ori: Vec2<f32>) -> EntityBuilder { self.ori = Some(ori); self }
    
    fn finish(self) -> Entity {
        // We use the .unwrap_or() method of Option<T> to revert to a default
        // value when an explicit value isn't given
        Entity {
            name: self.name.unwrap_or("Urist McMiner".to_string()),
            pos: self.pos.unwrap_or(vec3!(0.0, 0.0, 0.0)),
            vel: self.vel.unwrap_or(vec3!(0.0, 0.0, 0.0)),
            ori: self.ori.unwrap_or(vec2!(0.0, 0.0, 0.0)),
        }
    }
}

struct Entity {
    name: String,
    pos: Vec3<f32>,
    vel: Vec3<f32>,
    ori: Vec2<f32>,
}

impl Entity {
    fn make() -> EntityBuilder {
        EntityBuilder {
            name: None,
            pos: None,
            vel: None,
            ori: None,
        }
    }

    // etc...
}
```

Not all types require construction as complex as the above. Most types, particularly those that use only copy-able types, don't require an intermediate builder type.

```rust
struct Character {
    entity_uid: Uid,
    mode: CharacterMode,
    xp: u32,
}

impl Character {
    fn make(entity_uid: Uid) -> Character {
        // Construct with default copy-able types initially (clone-able types initiated
        // in this way would require an extra allocation, something we want to avoid)
        Character {
            entity_uid,
            mode: CharacterMode::Headless,
            xp: 0,
        }
    }

    fn with_mode(self, mode: CharacterMode) -> Character { self.mode = mode; self }
    fn with_xp(self, xp: u32) -> Character { self.xp = xp; self }

    // This method should still exist to retain a consistent API, despite having no
    // technical function
    fn finish(self) -> Character { self }
}
```

Note that it the `.finish()` method is still required to exist such that we may maintain a consistent API across the codebase.

# Drawbacks
[drawbacks]: #drawbacks

- More boilerplate code is required to be written by enginer developers

- There may potentially be a small run-time overhead for type construction (although in 90% of cases the compiler is intelligent enough to optimize the pattern out, particularly in optimized release builds)

# Rationale and alternatives
[alternatives]: #alternatives

- We keep to the typical `Type::new()` pattern used throughout Rust's standard library

- We build specific constructors for each construction case we require (the standard library's `Vec::with_capacity()` is an example of this)

# Prior art
[prior-art]: #prior-art

- This pattern is used extensively across many languages, particularly in functional languages and even more so in Rust

- This pattern has proven itself in other contexts to be readable, functional, flexible and easy to refactor

# Unresolved questions
[unresolved]: #unresolved-questions

There are currently no unresolved questions.